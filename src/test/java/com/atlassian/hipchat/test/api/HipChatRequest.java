package com.atlassian.hipchat.test.api;

import org.simpleframework.http.Method;
import org.simpleframework.http.Request;

import java.io.IOException;

import static com.google.common.base.Preconditions.checkNotNull;

public final class HipChatRequest
{
    private final String method;
    private final String url;
    private final String content;

    private HipChatRequest(String method, String url)
    {
        this(method, url, "");
    }

    private HipChatRequest(String method, String url, String content)
    {
        this.method = checkNotNull(method);
        this.url = checkNotNull(url);
        this.content = checkNotNull(content);
    }

    public static HipChatRequest get(String url)
    {
        return new HipChatRequest(Method.GET, url);
    }

    public static HipChatRequest post(String url, String content)
    {
        return new HipChatRequest(Method.POST, url, content);
    }

    boolean matches(Request request)
    {
        try
        {
            return request.getAddress().getPath().toString().equals(url)
                    && request.getMethod().equals(method)
                    && request.getContent().equals(content);
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
    }
}
