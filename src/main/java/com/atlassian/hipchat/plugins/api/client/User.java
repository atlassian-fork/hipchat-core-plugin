package com.atlassian.hipchat.plugins.api.client;

import com.google.common.base.Objects;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.Date;

import static com.atlassian.hipchat.plugins.core.util.DateMapper.toDate;

public class User
{
    private final long id;
    private final String name;
    private final String mentionName;
    private final String email;
    private final String title;
    private final String photoUrl;
    private final Date lastActive;
    private final Date created;
    private final String status;
    private final String statusMessage;
    private final boolean isGroupAdmin;
    private final boolean isDeleted;

    @JsonCreator
    public User(@JsonProperty("user_id") long id,
                @JsonProperty("name") String name,
                @JsonProperty("mention_name") String mentionName,
                @JsonProperty("email") String email,
                @JsonProperty("title") String title,
                @JsonProperty("photo_url") String photoUrl,
                @JsonProperty("last_active") long lastActive,
                @JsonProperty("created") long created,
                @JsonProperty("status") String status,
                @JsonProperty("status_message") String statusMessage,
                @JsonProperty("is_group_admin") int isGroupAdmin,
                @JsonProperty("is_deleted") int isDeleted)
    {

        this.id = id;
        this.name = name;
        this.mentionName = mentionName;
        this.email = email;
        this.title = title;
        this.photoUrl = photoUrl;
        this.lastActive = toDate(lastActive);
        this.created = toDate(created);
        this.status = status;
        this.statusMessage = statusMessage;
        this.isGroupAdmin = isGroupAdmin != 0;
        this.isDeleted = isDeleted != 0;
    }

    public long getId()
    {
        return id;
    }

    public String getName()
    {
        return name;
    }

    public String getMentionName()
    {
        return mentionName;
    }

    public String getEmail()
    {
        return email;
    }

    public String getTitle()
    {
        return title;
    }

    public String getPhotoUrl()
    {
        return photoUrl;
    }

    public Date getLastActive()
    {
        return lastActive;
    }

    public Date getCreated()
    {
        return created;
    }

    public String getStatus()
    {
        return status;
    }

    public String getStatusMessage()
    {
        return statusMessage;
    }

    public boolean isGroupAdmin()
    {
        return isGroupAdmin;
    }

    public boolean isDeleted()
    {
        return isDeleted;
    }

    @Override
    public String toString()
    {
        return Objects.toStringHelper(this)
                .add("id", id)
                .add("name", name)
                .add("mentionName", mentionName)
                .add("email", email)
                .add("title", title)
                .add("photoUrl", photoUrl)
                .add("lastActive", lastActive)
                .add("created", created)
                .add("status", status)
                .add("statusMessage", statusMessage)
                .add("isGroupAdmin", isGroupAdmin)
                .add("isDeleted", isDeleted)
                .toString();
    }
}
