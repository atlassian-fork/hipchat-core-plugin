package com.atlassian.hipchat.plugins.api.client;

public final class Message
{
    public static enum Format
    {
        TEXT("text"),
        HTML("html");

        public final String value;

        private Format(String value)
        {
            this.value = value;
        }
    }

    public static enum BackgroundColor
    {
        YELLOW("yellow"),
        RED("red"),
        GREEN("green"),
        PURPLE("purple"),
        GRAY("gray"),
        RANDOM("random");

        public final String value;

        private BackgroundColor(String value)
        {
            this.value = value;
        }
    }

    public static enum Status
    {
        SENT
    }
}
