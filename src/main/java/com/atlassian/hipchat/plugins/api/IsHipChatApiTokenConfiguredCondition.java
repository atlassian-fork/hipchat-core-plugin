package com.atlassian.hipchat.plugins.api;

import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

public final class IsHipChatApiTokenConfiguredCondition implements Condition
{
    private final HipChatConfigurationManager hipChatConfigurationManager;

    public IsHipChatApiTokenConfiguredCondition(HipChatConfigurationManager hipChatConfigurationManager)
    {
        this.hipChatConfigurationManager = checkNotNull(hipChatConfigurationManager);
    }

    @Override
    public void init(Map<String, String> params)
    {
    }

    @Override
    public boolean shouldDisplay(Map<String, Object> context)
    {
        return hipChatConfigurationManager.getApiToken().isDefined();
    }
}
