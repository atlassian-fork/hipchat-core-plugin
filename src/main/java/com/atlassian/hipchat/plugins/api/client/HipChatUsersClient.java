package com.atlassian.hipchat.plugins.api.client;

import com.atlassian.fugue.Either;
import com.atlassian.util.concurrent.Promise;

import java.util.List;

public interface HipChatUsersClient
{
    Promise<Either<ClientError, List<User>>> list();
}
