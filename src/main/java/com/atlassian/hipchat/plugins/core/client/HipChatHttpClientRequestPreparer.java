package com.atlassian.hipchat.plugins.core.client;

import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.atlassian.httpclient.api.Request;
import com.atlassian.util.concurrent.Effect;

import java.net.URI;

import static com.google.common.base.Preconditions.checkNotNull;

final class HipChatHttpClientRequestPreparer implements Effect<Request>
{
    private final HipChatConfigurationManager hipChatConfigurationManager;

    HipChatHttpClientRequestPreparer(HipChatConfigurationManager hipChatConfigurationManager)
    {
        this.hipChatConfigurationManager = checkNotNull(hipChatConfigurationManager);
    }

    @Override
    public void apply(Request request)
    {
        // make sure this is a request for a relative url
        if (request.getUri().toString().matches("^[\\w]+:.*"))
        {
            throw new IllegalStateException("Absolute request URIs are not supported for host requests");
        }



        final StringBuilder uriBuf = new StringBuilder();
        uriBuf.append(hipChatConfigurationManager.getApiBaseUrl());
        if (uriBuf.charAt(uriBuf.length() - 1) != '/')
        {
            uriBuf.append("/");
        }
        uriBuf.append("v1");
        uriBuf.append(request.getUri());

        // append the auth_token
        if (uriBuf.indexOf("&auth_token=") < 0 && uriBuf.indexOf("?auth_token=") < 0)
        {
            uriBuf.append(uriBuf.indexOf("?") > 0 ? '&' : '?')
                    .append("auth_token")
                    .append('=')
                    .append(hipChatConfigurationManager.getApiToken().getOrElse(""));
        }

        request.setUri(URI.create(uriBuf.toString()));
    }
}
