package com.atlassian.hipchat.plugins.core.client;

import com.atlassian.fugue.Either;
import com.atlassian.hipchat.plugins.api.client.ClientError;
import com.atlassian.hipchat.plugins.api.client.HipChatUsersClient;
import com.atlassian.hipchat.plugins.api.client.User;
import com.atlassian.httpclient.api.HttpClient;
import com.atlassian.util.concurrent.Promise;
import org.codehaus.jackson.type.TypeReference;

import java.util.List;

import static com.atlassian.hipchat.plugins.core.client.ClientUtils.parseJson;

public class HipChatUsersClientImpl implements HipChatUsersClient
{
    private final HttpClient httpClient;

    public HipChatUsersClientImpl(HttpClient httpClient)
    {
        this.httpClient = httpClient;
    }

    @Override
    public Promise<Either<ClientError, List<User>>> list()
    {
        return httpClient.newRequest("/users/list").get().<Either<ClientError, List<User>>>transform()
                .ok(parseJson("users", new TypeReference<List<User>>(){}))
                .others(ClientUtils.<List<User>>error())
                .fail(ClientUtils.<List<User>>throwable())
                .toPromise();
    }
}
