package com.atlassian.hipchat.plugins.core.client;

import com.atlassian.event.api.EventPublisher;
import com.atlassian.fugue.Either;
import com.atlassian.fugue.Option;
import com.atlassian.hipchat.plugins.api.client.ClientError;
import com.atlassian.hipchat.plugins.api.client.HipChatClient;
import com.atlassian.hipchat.plugins.api.client.HipChatRoomsClient;
import com.atlassian.hipchat.plugins.api.client.HipChatUsersClient;
import com.atlassian.hipchat.plugins.api.config.HipChatConfigurationManager;
import com.atlassian.httpclient.apache.httpcomponents.DefaultHttpClient;
import com.atlassian.httpclient.api.factory.HttpClientOptions;
import com.atlassian.sal.api.ApplicationProperties;
import com.atlassian.util.concurrent.Promise;
import com.google.common.base.Function;
import com.google.common.base.Suppliers;
import org.springframework.beans.factory.DisposableBean;

import static com.atlassian.hipchat.plugins.core.client.ClientUtils.constantRight;
import static com.google.common.base.Preconditions.checkNotNull;

public final class HipChatClientImpl implements HipChatClient, DisposableBean
{
    private final DefaultHttpClient<Void> httpClient;

    public HipChatClientImpl(EventPublisher eventPublisher, ApplicationProperties applicationProperties, HipChatConfigurationManager hipChatConfigurationManager)
    {
        this.httpClient = new DefaultHttpClient<Void>(
                checkNotNull(eventPublisher),
                checkNotNull(applicationProperties),
                new NoOpThreadLocalContextManager(),
                getHttpOptions(checkNotNull(hipChatConfigurationManager)));
    }

    @Override
    public Promise<Either<ClientError, Boolean>> canAuthenticate(Option<String> token)
    {
        final String uri = token.fold(
                Suppliers.ofInstance("/"),
                new Function<String, String>()
                {
                    @Override
                    public String apply(String t)
                    {
                        return "/?auth_token=" + t;
                    }
                }
        );

        return httpClient.newRequest(uri).get().<Either<ClientError, Boolean>>transform()
                .ok(constantRight(Boolean.TRUE))
                .unauthorized(constantRight(Boolean.FALSE))
                .others(ClientUtils.<Boolean>error())
                .fail(ClientUtils.<Boolean>throwable())
                .toPromise();
    }

    @Override
    public HipChatRoomsClient rooms()
    {
        return new HipChatRoomsClientImpl(httpClient);
    }

    @Override
    public HipChatUsersClient users()
    {
        return new HipChatUsersClientImpl(httpClient);
    }

    private HttpClientOptions getHttpOptions(HipChatConfigurationManager hipChatConfigurationManager)
    {
        HttpClientOptions options = new HttpClientOptions();
        options.setThreadPrefix("hipchat-client");
        options.setRequestPreparer(new HipChatHttpClientRequestPreparer(hipChatConfigurationManager));
        return options;
    }

    @Override
    public void destroy() throws Exception
    {
        httpClient.destroy();
    }
}
