package com.atlassian.hipchat.plugins.core.client;

import com.atlassian.httpclient.spi.ThreadLocalContextManager;

public final class NoOpThreadLocalContextManager implements ThreadLocalContextManager<Void>
{
    @Override
    public Void getThreadLocalContext()
    {
        return null;
    }

    @Override
    public void setThreadLocalContext(Void aVoid)
    {
    }

    @Override
    public void resetThreadLocalContext()
    {
    }
}
